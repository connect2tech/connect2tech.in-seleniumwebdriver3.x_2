package com.c2t.apachepoi;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

//import statements
public class AppendExcel {
	public static void main(String[] args) throws Exception {
		// Blank workbook
		FileInputStream myxls = new FileInputStream(new File("poi-testt.xlsx"));
		XSSFWorkbook workbook = new XSSFWorkbook(myxls);

		// Create a blank sheet
		XSSFSheet sheet = workbook.getSheetAt(0);

		int lastRowNum = sheet.getLastRowNum();
		Row row = sheet.createRow(++lastRowNum);
		row.createCell(0).setCellValue("Adding data-0");
		row.createCell(1).setCellValue("Adding data-1");

		try {
			// Write the workbook in file system
			FileOutputStream out = new FileOutputStream(new File("poi-testt.xlsx"));
			workbook.write(out);
			out.close();
			System.out.println("howtodoinjava_demo.xlsx written successfully on disk.");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// XSSFWorkbook workbook = new XSSFWorkbook();
	// XSSFSheet sheet = workbook.createSheet("Employee Data");
	// Row row = sheet.createRow(rowCount);
	// Cell cell = row.createCell(colCount);
	// cell.setCellValue(data[rowCount][colCount]);
	// FileOutputStream out = new FileOutputStream(new
	// File("howtodoinjava_out.xlsx"));
	// workbook.write(out);
	// out.close();
}
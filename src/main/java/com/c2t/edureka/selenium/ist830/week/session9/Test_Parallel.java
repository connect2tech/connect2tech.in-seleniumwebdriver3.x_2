package com.c2t.edureka.selenium.ist830.week.session9;

import org.testng.annotations.Test;

public class Test_Parallel {

	@Test
	public void openPage2() {
		System.out.println("openPage");
		long id1 = Thread.currentThread().getId();
		System.out.println("Test_Parallel/id1="+id1);
	}

	@Test
	public void enterId() {
		System.out.println("enterId");
		
		long id2 = Thread.currentThread().getId();
		System.out.println("Test_Parallel/id2="+id2);
	}
	
	@Test
	public void enterId2() {
		System.out.println("enterId");
		
		long id3 = Thread.currentThread().getId();
		System.out.println("Test_Parallel/id3="+id3);
	}

}

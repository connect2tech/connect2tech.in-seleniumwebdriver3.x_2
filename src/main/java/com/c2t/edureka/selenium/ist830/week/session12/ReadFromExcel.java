package com.c2t.edureka.selenium.ist830.week.session12;

import java.io.File;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class ReadFromExcel {
	public static void main(String[] args) {

		try {
			File f = new File("D:/nchaurasia/Automation-Architect/connect2tech.in-SeleniumWebDriver3.x_2/DDT.xlsx");
			XSSFWorkbook workbook = new XSSFWorkbook(f);

			XSSFSheet sheet = workbook.getSheet("Employee Data");

			int rowCount = sheet.getPhysicalNumberOfRows();
			System.out.println(rowCount);
			
			for(int i=0;	i<rowCount;	i++){
				Row row = sheet.getRow(i);
				
				
				int cellCount = row.getPhysicalNumberOfCells();
				
				for(int j=0;	j<cellCount;	j++){
					Cell c = row.getCell(j);
					System.out.println(c.toString());
				}
				
				System.out.println("----------------------------");
				
			}
			
			
			/*
			Iterator<Row> rows = sheet.rowIterator();
			
			while(rows.hasNext()){
				Row row = rows.next();
				
				Iterator<Cell> cells = row.cellIterator();
				
				while(cells.hasNext()){
					Cell cell = cells.next();
					System.out.println(cell.toString());
				}
				
				System.out.println("-------------------");
				
			}*/
		
		} catch (Exception e) {
			System.out.println(e);
		}

	}
}

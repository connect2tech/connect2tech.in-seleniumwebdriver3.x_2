package com.c2t.edureka.selenium.ist830.week.session12;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelFile {

	public Sheet readExcel(String filePath, String fileName, String sheetName)
			throws Exception {
		File file = new File(filePath + "\\" + fileName);
		
		Workbook wb = null;
		wb = new XSSFWorkbook(file);
		// Read sheet inside the workbook by its name
		Sheet guru99Sheet = wb.getSheet(sheetName);
		return guru99Sheet;
	}
	
}
package com.c2t.edureka.wknd.mar19.ist20_30.selenium4;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class EventHandling {

	public static void main(String[] args) {
		String url = "https://www.facebook.com";
		// declaration and instantiation of objects/variables
		System.setProperty("webdriver.chrome.driver",
				"D:/nchaurasia/Automation-Architect/Java-Selenium-Edureka-Feb-2019/chrome-driver-2.45/chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);

		driver.get(url);
		
		WebElement email = driver.findElement(By.name("email"));

		Actions actions = new Actions(driver);
		actions.moveToElement(email).click().keyDown(Keys.SHIFT).sendKeys("hello").doubleClick().build().perform();
		
		/*Actions action1 = actions.moveToElement(email);
		Actions action2 = action1.click();
		Actions action3 = action2.keyDown(Keys.SHIFT);
		Actions action4 = action3.sendKeys("naresh");
		Actions action5 = action4.doubleClick();
		action5.build().perform();*/
	}
}
package com.c2t.edureka.wknd.mar19.ist20_30.selenium3;

import java.util.HashSet;
import java.util.Iterator;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class SetJava {
	public static void main(String[] args) {
		HashSet<String> set = new HashSet<String>();
		set.add("A");
		set.add("B");
		set.add("A");

		System.out.println(set);

		Iterator<String> iter = set.iterator();

		while (iter.hasNext()) {
			String s = iter.next();

			if (s.equals("A")) {
				System.out.println("A is present");
			} else {
				System.out.println("A not present..");
			}
		}

	}
}

package com.c2t.edureka.wknd.mar19.ist20_30.selenium5;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.FindBy;
import java.util.List;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/ 
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class FbLoginPomPageFactory {
	
	WebDriver d;
	
	@FindBy(id="pass")
	List<WebElement> password; // findElements(By.id("pass"))
	
	@FindBy(xpath="//*[@id='email']")
	WebElement email1;
	
	@FindBy(id="loginbutton")
	WebElement button;
	
	public FbLoginPomPageFactory(WebDriver driver){
		//d = driver;
		PageFactory.initElements(driver, this);
		System.out.println("this="+this);
	}
	
	public void setEmail(String s1){
		
		email1.sendKeys("abc");
	}
	
	public void setPassword(String s2){
		//password.sendKeys("password");
	}
	
	public void clickLogin(){
		button.click();
		
	}
}

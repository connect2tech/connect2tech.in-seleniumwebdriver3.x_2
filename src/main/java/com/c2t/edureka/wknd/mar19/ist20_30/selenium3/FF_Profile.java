package com.c2t.edureka.wknd.mar19.ist20_30.selenium3;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class FF_Profile {

	public static void main(String[] args) {
		String url = "file:///D:/nchaurasia/Automation-Architect/connect2tech.in-SeleniumWebDriver3.x_2/src/main/resources/LocatingMultipleElements.html";
		// declaration and instantiation of objects/variables
		/*System.setProperty("webdriver.chrome.driver",
				"D:/nchaurasia/Automation-Architect/Java-Selenium-Edureka-Feb-2019/chrome-driver-2.45/chromedriver.exe");*/
		System.setProperty("webdriver.gecko.driver", "D:/nchaurasia/Automation-Architect/connect2tech.in-SeleniumWebDriver3.x_2/geckodriver-v0.24.0-win64/geckodriver.exe");
		
		ProfilesIni profile = new ProfilesIni();
		FirefoxProfile ffProfile = profile.getProfile("Testing");
		
		FirefoxOptions ff_options = new FirefoxOptions();
		ff_options.setProfile(ffProfile);	
		
		WebDriver driver = new FirefoxDriver();
		driver.get(url);
		
		
		
		
		
	}

}

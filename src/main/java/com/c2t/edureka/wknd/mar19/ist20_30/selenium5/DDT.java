package com.c2t.edureka.wknd.mar19.ist20_30.selenium5;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class DDT {

	WebDriver driver;

	@BeforeTest
	public void before() {
		String url = "http://cookbook.seleniumacademy.com/bmicalculator.html";
		// declaration and instantiation of objects/variables
		System.setProperty("webdriver.chrome.driver",
				"D:/nchaurasia/Automation-Architect/Java-Selenium-Edureka-Feb-2019/chrome-driver-2.45/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);

		driver.get(url);
	}

	/*
	 * Mark a method as supplying data for a test method. The data provider name
	 * defaults to method name. The annotated method must return an Object[][]
	 * where each Object[] can be assigned the parameter list of the test
	 * method. The @Test method that wants to receive data from this
	 * DataProvider needs to use a dataProvider name equals to the name of this
	 * annotation.
	 */
	@DataProvider
	public String[][] getData() {
		String[][] data = {

			{ "163", "50" }
				
		};

		return data;
	}

	@Test(dataProvider="getData")
	public void test1(String val1, String val2) {
		WebElement heightField = driver.findElement(By.name("heightCMS"));
		heightField.clear();
		heightField.sendKeys(val1);

		WebElement weightField = driver.findElement(By.name("weightKg"));
		weightField.clear();
		weightField.sendKeys(val2);

		WebElement calculateButton = driver.findElement(By.id("Calculate"));
		calculateButton.click();
	}

}

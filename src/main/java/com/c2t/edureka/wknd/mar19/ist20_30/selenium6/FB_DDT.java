package com.c2t.edureka.wknd.mar19.ist20_30.selenium6;

import java.io.File;
import java.io.FileInputStream;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class FB_DDT {

	WebDriver driver;

	public String[][] readDataFromExcel() throws Exception {

		String myData[][] = new String[2][2];

		File file = new File("DDT.xlsx");
		FileInputStream fileInputStream = new FileInputStream(file);

		XSSFWorkbook workbook = new XSSFWorkbook(fileInputStream);
		XSSFSheet sheet = workbook.getSheet("Employee Data");

		Iterator<Row> rows = sheet.rowIterator();

		int rowCount = 0;

		while (rows.hasNext()) {

			Row row = rows.next();

			Iterator<Cell> cells = row.cellIterator();

			int colCount = 0;
			while (cells.hasNext()) {

				Cell cellValue = cells.next();
				//System.out.println("cellValue=" + cellValue);

				myData[rowCount][colCount] = cellValue.toString();
				++colCount;

			}

			++rowCount;

			System.out.println("------------------------------------");

		}

		return myData;

	}

	@BeforeMethod
	public void before() {
		String url = "https://www.facebook.com/";
		// declaration and instantiation of objects/variables
		System.setProperty("webdriver.chrome.driver",
				"D:/nchaurasia/Automation-Architect/Java-Selenium-Edureka-Feb-2019/chrome-driver-2.45/chromedriver.exe");

	}

	@DataProvider
	public String[][] data() throws Exception {
		String idPwd[][] = readDataFromExcel();

		return idPwd;
	}

	@Test(dataProvider = "data")
	public void test1(String user, String pwd) {
		
		driver = new ChromeDriver();
		driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
		driver.get("https://www.facebook.com/");

		driver.findElement(By.name("email")).sendKeys(user);
		driver.findElement(By.id("pass")).sendKeys(pwd);

		driver.close();
		/*
		System.out.println(user);
		System.out.println(pwd);*/
	}

}
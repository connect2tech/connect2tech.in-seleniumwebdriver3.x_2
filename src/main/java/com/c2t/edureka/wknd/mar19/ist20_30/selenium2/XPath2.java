package com.c2t.edureka.wknd.mar19.ist20_30.selenium2;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class XPath2 {

	public static void main(String[] args) {
		String url = "https://www.facebook.com/";
		// declaration and instantiation of objects/variables
		System.setProperty("webdriver.chrome.driver",
				"D:/nchaurasia/Automation-Architect/Java-Selenium-Edureka-Feb-2019/chrome-driver-2.45/chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

		// opened the url in browser
		driver.get(url);

		// String str =
		// driver.findElement(By.xpath("//div[starts-with(text(),'Facebook')]")).getText();
		// System.out.println(str);

		// driver.findElement(By.xpath("//input[@type='email' and
		// @class='inputaaa']")).sendKeys("Java Selenium");

		driver.findElement(By.xpath("input[starts-with(@type,'askdjfalj')]")).sendKeys("Java Selenium");
	}

}

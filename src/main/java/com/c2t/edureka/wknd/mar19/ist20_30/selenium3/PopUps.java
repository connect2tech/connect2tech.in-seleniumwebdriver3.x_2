package com.c2t.edureka.wknd.mar19.ist20_30.selenium3;

import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/
 * @Profile : https://goo.gl/2mCt6v
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class PopUps {

	public static void main(String[] args) {
		String url = "http://demo.guru99.com/popup.php";
		// declaration and instantiation of objects/variables
		System.setProperty("webdriver.chrome.driver",
				"D:/nchaurasia/Automation-Architect/Java-Selenium-Edureka-Feb-2019/chrome-driver-2.45/chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get(url);

		driver.findElement(By.linkText("Click Here")).click();
		
		String parent = driver.getWindowHandle();
		Set <String> allWindows = driver.getWindowHandles();
		
		System.out.println(parent);
		System.out.println(allWindows);
		
		Iterator<String> iter = allWindows.iterator();
		
		while(iter.hasNext()){
			String temp = iter.next();
			
			if(!temp.equals(parent)){
				driver.switchTo().window(temp);
				driver.findElement(By.name("emailid")).sendKeys("message4naresh@gmail.com");
				driver.close();
			}
		}
		
	}

}

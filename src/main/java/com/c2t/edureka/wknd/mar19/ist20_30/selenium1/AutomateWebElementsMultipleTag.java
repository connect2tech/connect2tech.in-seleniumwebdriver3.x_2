package com.c2t.edureka.wknd.mar19.ist20_30.selenium1;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class AutomateWebElementsMultipleTag {
	public static void main(String[] args) {
		String url = "file:///D:/nchaurasia/Automation-Architect/connect2tech.in-SeleniumWebDriver3.x_2/src/main/resources/LocatingMultipleElements.html";
		// declaration and instantiation of objects/variables
		System.setProperty("webdriver.chrome.driver",
				"D:/nchaurasia/Automation-Architect/Java-Selenium-Edureka-Feb-2019/chrome-driver-2.45/chromedriver.exe");
		WebDriver driver = new ChromeDriver();

		// opened the url in browser
		driver.get(url);

		List <WebElement> l = driver.findElements(By.tagName("input"));
		System.out.println(l.size());
		
		for(int i=0;	i<l.size();		i++){
			
				WebElement element = l.get(i);
				
				if(i == 1){
					element.clear();
					element.sendKeys("i am last name");
				}
		}

	}
}

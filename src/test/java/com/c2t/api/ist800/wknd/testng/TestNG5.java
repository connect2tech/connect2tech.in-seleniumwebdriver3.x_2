package com.c2t.api.ist800.wknd.testng;

import org.testng.annotations.Test;

public class TestNG5 {
	
	@Test(priority=-1, enabled=false)
	public void createFbAccount(){
		System.out.println("TestNG5/createFbAccount");
	}

	@Test//default 0
	public void openFbUrl_1() {
		System.out.println("TestNG5/test3");
	}
	
	@Test(priority=1)
	public void openFbUrl_2() {
		System.out.println("TestNG5/test3");
	}

	@Test(priority=2)
	public void enterUserNamePwd() {
		System.out.println("TestNG5/test1");
	}

	@Test(priority=-3)
	public void clickLoginButton() {
		System.out.println("TestNG5/test2");
	}

}

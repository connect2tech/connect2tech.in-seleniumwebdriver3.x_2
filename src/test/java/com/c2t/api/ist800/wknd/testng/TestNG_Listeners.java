package com.c2t.api.ist800.wknd.testng;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

@Listeners({TestNgListener.class})
public class TestNG_Listeners {

	@Test
	public void test3() {
		System.out.println("test3");
	}

	
	@Test
	public void test4() {
		throw new RuntimeException("Error");
	}
}

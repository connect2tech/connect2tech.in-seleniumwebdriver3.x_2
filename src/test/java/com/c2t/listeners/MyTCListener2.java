package com.c2t.listeners;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import java.util.Date;

public class MyTCListener2 implements ITestListener{

	@Override
	public void onTestStart(ITestResult result) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onTestSuccess(ITestResult result) {
		
		String m1 = result.getMethod().getMethodName();
		System.out.println(m1);
		
	}

	@Override
	public void onTestFailure(ITestResult result){
		// TODO Auto-generated method stub
		String m1 = result.getMethod().getMethodName();
		System.out.println("-----------------------");
		System.out.println(m1);
		
		WebDriver driver1 = Singleton.getSingleton().getD();
		
		TakesScreenshot sc = (TakesScreenshot) driver1;
		File screenShot = sc.getScreenshotAs((OutputType.FILE));
		
		String s = "screen"+new Date().getTime();
		
		try {
			FileUtils.copyFile(screenShot, new File(s+".jpg"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	@Override
	public void onTestSkipped(ITestResult result) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStart(ITestContext context) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onFinish(ITestContext context) {
		// TODO Auto-generated method stub
		
	}
	
}

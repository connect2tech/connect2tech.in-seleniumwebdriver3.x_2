package com.c2t.listeners;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;


public class MyTC2 {
	
	WebDriver driver;
	
	
	@BeforeClass
	public void before(){
		String url = "https://www.google.com/";
		// declaration and instantiation of objects/variables
		System.setProperty("webdriver.firefox.marionette", "D:/nchaurasia/Automation-Architect/connect2tech.in-SeleniumWebDriver3.x_2/driver/geckodriver.exe");
		
		driver = new FirefoxDriver();
		driver.get(url);
		
		Singleton s = Singleton.getSingleton();
		s.setD(driver);
	}
	
	@Test
	public void test1(){
		System.out.println("MyTc1..");
	}
	
	@Test
	public void test2(){
		System.out.println("MyTc2..");
	}
	
}

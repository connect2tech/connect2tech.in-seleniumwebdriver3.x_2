package in.connect2tech.cogni;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Locators {

	private WebDriver driver;

	@BeforeClass
	public void setUp() {
		System.setProperty("webdriver.chrome.driver",
				"D:\\naresh.chaurasia\\Automation-Architect\\connect2tech\\connect2tech.in-seleniumwebdriver3.x_2\\chrome-cogni\\chromedriver.exe");
		driver = new ChromeDriver();
		// driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

		driver.get("https://www.etsy.com/");
		driver.manage().window().maximize();
	}

	@Test
	public void t1() {
		long time1 = new Date().getTime();

		try {
			WebDriverWait wait = new WebDriverWait(driver, 5);
			wait.until(ExpectedConditions.presenceOfElementLocated(By.id("search-query")));
			driver.findElement(By.id("search-query")).sendKeys("hello");
		} catch (Exception e) {
			System.out.println(e);
			
			WebDriverWait wait = new WebDriverWait(driver, 5);
			wait.until(ExpectedConditions.presenceOfElementLocated(By.id("global-enhancements-search-query")));
			driver.findElement(By.id("global-enhancements-search-query")).sendKeys("hello");
			
		}

		long time2 = new Date().getTime();

		System.out.println("time2-time1====================>" + (time2 - time1));
	}

}

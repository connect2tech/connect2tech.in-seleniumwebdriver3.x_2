package in.connect2tech.testng.basic;

import java.lang.reflect.Method;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class TestNgDataProvider1 {

	@DataProvider
	public String[][] myData(Method method) {

		String data[][] = { { "A", "B" }, { "C", "D" } };
		
		System.out.println("method=>"+method);

		return data;

	}

	@Test(dataProvider = "myData")
	public void test1(String v1, String v2) {
		System.out.println("test1");
		System.out.println(v1);
		System.out.println(v2);
	}

}
